;(function ($, window, document, undefined) {

    function resizeStages() {
        var windowWidth = $(window).width(),
            windowHeight = $(window).height();

        $('.world').each(function () {
            var world = $(this);

            world.find('.stage').each(function () {
                var stage = $(this),
                    height = windowHeight;

                stage.find('.viewport-width').css('width', windowWidth + 'px');

                stage.find('.content-layer').each(function () {
                    var layer = $(this),
                        content = layer.find('> .content'),
                        contentHeight;

                    content.css('height', 'auto');
                    contentHeight = content.outerHeight();
                    content.css('height', '100%');

                    layer.css('height', contentHeight + 'px');
                    height = contentHeight > height ? contentHeight : height;
                });

                stage.css('height', height + 'px');
            });

            world.find('.stage .content-layer').each(function () {
                var layer = $(this),
                    stage = layer.closest('.stage');

                layer.css('height', stage.height() + 'px');
            });
        });
    }


    function createStickyScrollHandler() {
        var stages = [];

        $('.sticky-stage').each(function () {
            stages.push($(this));
        });

        return function () {
            var scrollTop = $(window).scrollTop(),
                windowHeight = $(window).height();

            for (var i in stages) {
                var stage = stages[i],
                    height = stage.outerHeight(),
                    offset = stage.offset().top,
                    translate = stage.data('translate-y') || 0,
                    heightDelta = height - windowHeight;

                if (heightDelta < 0) {
                    heightDelta = 0;
                }

                if (offset - translate + heightDelta < scrollTop) {
                    translate = scrollTop - (offset - translate) - heightDelta;
                } else {
                    translate = 0;
                }

                stage.css('transform', 'translateY(' + translate + 'px)');
                stage.data('translate-y', translate);
            }
        };
    }


    $(function () {
        $('body').removeClass('no-js').addClass('js');

        resizeStages();
        $(window).on('resize', resizeStages);

        var scrollHandler = createStickyScrollHandler();
        if (typeof requestAnimationFrame != 'undefined') {
            var frameCallback = function () {
                scrollHandler();
                requestAnimationFrame(frameCallback);
            };

            frameCallback();
        } else {
            setInterval(scrollHandler, 50);
        }
    });

})(jQuery, this, this.document);
